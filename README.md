# A 2-player tic-tac-toe game without a game server

This two-player tic-tac-toe game works without a game server. The game runs
solely on the players' devices. It has been tested on Firefox 116 on Linux and
Firefox 117 on Android.

Live demo: https://stefanha.gitlab.io/tic-tac-toe-p2p/

The game itself is not the point. The point is to show an application where
users communicate without a server, API keys, accounts, paid services, or other
centralized infrastructure. It still relies on public STUN servers and nostr
relays to set up a peer-to-peer WebRTC DataChannel.

This is a prototype. A real application should not use NIP-04 because it is
wasteful for the relay to store the messages. A nostr event "kind" value should
be chosen from the ephemeral range (20,000 - 30,000). Perhaps a NIP should be
written to standardize the WebRTC signalling protocol.

## How the peer-to-peer connection is set up
There are two actors: "A" hosts a game and "B" joins A's game.

1. A: Subscribe to encrypted direct messages (NIP-04) on nostr relay
2. B: Subscribe to encrypted direct messages from A and then create a WebRTC offer and send it to A inside an encrypted direct message
3. A: Create a WebRTC answer and send it to B inside an encrypted direct message
4. A & B: Exchange ICE candidates over encrypted direct messages
5. WebRTC DataChannel opens and nostr is no longer needed

This process only works well when the nostr relay delivers events in a timely fashion. If the relay is slow then WebRTC negotiation times out.

Using encrypted direct messages prevents the relays from interfering with
WebRTC negotiation and man-in-the-middle attacks.

## The signalling protocol
The signalling protocol consists of JSON type/value objects:
- { type: 'offer', value: ...WebRTC offer... }
- { type: 'answer', value: ...WebRTC offer... }
- { type: 'iceCandidates', value: [...ICE candidates...] }

## The game protocol
The game protocol consists of a single JSON type/value object:
- { type: 'move', value: { x, y } }
