// SPDX-License-Identifier: MIT
//
// Tic-Tac-Toe game over a WebRTC DataChannel using nostr for signalling
// Copyright (C) 2023 Stefan Hajnoczi <stefanha@gmail.com>
//
// This two-player tic-tac-toe game works without a game server. The game runs
// solely on the players' devices. It has been tested on Firefox 116 on Linux
// and Firefox 117 on Android.
//
// One player "hosts" a game and shares a link with the other player. The other
// player "joins" the game using the link containing the host's nostr pubkey.
// Encrypted direct messages (NIP-04) are exchanged to set up a peer-to-peer
// WebRTC connection.
//
// The peer-to-peer connection is used to exchange players' moves. Nostr is not
// used once the WebRTC connection has been established.
//
// This is a prototype. It is not 100% reliable (reload and try again if it
// doesn't work).

// The keys are used for end-to-end encryption in the nostr protocol. The
// public key of the player hosting the game is also used as the game id that
// the other player uses to connect.
const privkey = window.NostrTools.generatePrivateKey()
const pubkey = window.NostrTools.getPublicKey(privkey)

// There are many public nostr relays with different supported features and
// latency. RTCPeerConnection has a timeout while ICE candidates are exchanged,
// so we need a nostr relay that delivers events within a reasonable amount of
// time.
const relay = window.NostrTools.relayInit('wss://relayable.org/')

// A set of event ids to skip duplicate nostr events. Just in case.
const seenEvents = new Set()

// The WebRTC connection that will be established using nostr as the signalling
// mechanism.
let pc = new RTCPeerConnection({iceServers: [{urls: 'stun:stun.l.google.com:19302'}]})

// The game protocol runs over this RTCDataChannel.
let channel = null

// Is it our turn in the game?
let myTurn = false

// Who uses X and O?
let mySymbol = null
let opponentSymbol = null

// Switch between different UI states (screens)
function switchUI(from, to) {
    document.getElementById(from).className = 'd-none'
    document.getElementById(to).className = ''
}

// Is the given UI state currently being shown?
function inUI(id) {
    return document.getElementById(id).className !== 'd-none'
}

// Publish an encrypted direct message (NIP-04) on nostr
async function publish(toPubkey, message) {
    console.log('publishing message', message)
    const ciphertext = await window.NostrTools.nip04.encrypt(privkey, toPubkey, message)
    let event = {
        kind: 4,
        pubkey: pubkey,
        created_at: Math.floor(Date.now() / 1000),
        tags: [['p', toPubkey]],
        content: ciphertext,
    }
    const signedEvent = window.NostrTools.finishEvent(event, privkey)
    await relay.publish(signedEvent)
    console.log('published', signedEvent)
}

// Gather ICE candidates and send them to the other player
function setUpICECandidateExchange(opponentPubkey) {
    let iceCandidates = []
    pc.onicecandidate = async ({ candidate }) => {
        // Don't do trickle ICE because nostr latency can be high
        if (candidate === null) {
            await publish(opponentPubkey, JSON.stringify({type: 'iceCandidates', value: iceCandidates}))
        } else {
            iceCandidates.push(candidate.toJSON())
        }
    }
    pc.onconnectionstatechange = event => console.log('connectionstatechange', event)
}

function addICECandidates(candidates) {
    console.log('got ice candidates', candidates)
    for (const candidate of candidates) {
        pc.addIceCandidate(new RTCIceCandidate(candidate))
    }
}

// Receive a WebRTC offer from the other player and negotiate a data channel
function hostGameSubscribeNostrEvents() {
    let sub = relay.sub([
        {
            kinds: [4],
            '#p': [pubkey],
            limit: 0,
        }
    ])
    sub.on('event', async event => {
        if (seenEvents.has(event.id)) {
            return
        }
        seenEvents.add(event.id)

        console.log('got nostr event', event)

        const opponentPubkey = event.pubkey
        setUpICECandidateExchange(opponentPubkey)

        const content = JSON.parse(await window.NostrTools.nip04.decrypt(privkey, opponentPubkey, event.content))
        console.log('got content', content)

        if (content.type == 'offer') {
            pc.ondatachannel = event => {
                sub.unsub()
                console.log('on data channel', event)
                channel = event.channel
                switchUI('wait-for-opponent', 'game')
                startGame(true)
            }

            pc.setRemoteDescription(new RTCSessionDescription(content.value))
            const answer = await pc.createAnswer()
            await publish(opponentPubkey, JSON.stringify({type: 'answer', value: answer}))
            await pc.setLocalDescription(answer)
        } else if (content.type == 'iceCandidates') {
            addICECandidates(content.value)
        }
    })
}

function hostGame() {
    document.getElementById('our-pubkey').value = pubkey
    document.getElementById('join-link').href = window.location.href.split('#')[0] + '#' + pubkey
    switchUI('host-or-join', 'wait-for-opponent')

    if (relay.status === 1) {
        hostGameSubscribeNostrEvents()
    }
}

function joinOpponentPubkey() {
    switchUI('host-or-join', 'join-opponent-pubkey')
}

// Send a WebRTC offer to the other player and negotiate a data channel
async function joinGamePublishNostrEvent() {
    channel = pc.createDataChannel('channel')

    const offer = await pc.createOffer()
    await pc.setLocalDescription(offer)
    console.log('offer', offer)

    const opponentPubkey = document.getElementById('opponent-pubkey').value

    let sub = relay.sub([
        {
            kinds: [4],
            authors: [opponentPubkey],
            '#p': [pubkey],
            limit: 0,
        }
    ])
    sub.on('event', async event => {
        if (seenEvents.has(event.id)) {
            return
        }
        seenEvents.add(event.id)

        console.log('got nostr event', event)
        const content = JSON.parse(await window.NostrTools.nip04.decrypt(privkey, opponentPubkey, event.content))
        console.log('got content', content)

        if (content.type === 'answer') {
            setUpICECandidateExchange(opponentPubkey)
            channel.onopen = () => {
                sub.unsub()
                console.log('channel open')
                switchUI('joining', 'game')
                startGame(false)
            }
            pc.setRemoteDescription(new RTCSessionDescription(content.value))
        } else if (content.type == 'iceCandidates') {
            addICECandidates(content.value)
        }
    })

    await publish(opponentPubkey, JSON.stringify({type: 'offer', value: offer}))
}

function joining() {
    const opponentPubkey = document.getElementById('opponent-pubkey').value
    if (opponentPubkey === '') {
        return
    }

    switchUI('join-opponent-pubkey', 'joining')

    if (relay.status === 1) {
        joinGamePublishNostrEvent()
    }
}

function setMyTurn(val) {
    myTurn = val
    document.getElementById('turn-info').innerHTML = val ?
        'Click on a cell to make your move' :
        'Waiting for your opponent to make their move'
}

function clearGameState() {
    for (let y = 0; y < 3; y++) {
        for (let x = 0; x < 3; x++) {
            document.getElementById(`x${x}y${y}`).innerHTML = ''
        }
    }
}

function startGame(firstPlayer) {
    clearGameState()

    mySymbol = firstPlayer ? 'X' : 'O'
    opponentSymbol = firstPlayer ? 'O' : 'X'
    setMyTurn(firstPlayer)

    channel.onclose = () => console.log('channel close')
    channel.onmessage = event => {
        const data = JSON.parse(event.data)
        if (data.type === 'move') {
            console.log(`received move ${data.value.x} ${data.value.y}`)

            if (!performMove(data.value.x, data.value.y, opponentSymbol)) {
                return // invalid move
            }

            checkGameOver()
        }
    }
}

function checkGameOver() {
    const rows = []
    for (let y = 0; y < 3; y++) {
        const row = []
        for (let x = 0; x < 3; x++) {
            row.push(document.getElementById(`x${x}y${y}`).innerHTML)
        }
        rows.push(row)
    }

    const cols = rows[0].map((_, i) => rows.map(row => row[i]))
    const diags = [[rows[0][0], rows[1][1], rows[2][2]],
                   [rows[0][2], rows[1][1], rows[2][0]]]

    for (let seq of rows.concat(cols).concat(diags)) {
        if (seq.every(elem => elem === mySymbol)) {
            document.getElementById('game-over-message').innerHTML = 'You win!'
            switchUI('game', 'game-over')
            return
        } else if (seq.every(elem => elem === opponentSymbol)) {
            document.getElementById('game-over-message').innerHTML = 'You lose!'
            switchUI('game', 'game-over')
            return
        }
    }
}

// Returns true if the move was valid, false otherwise
function performMove(x, y, symbol) {
    const myMove = symbol === mySymbol
    if (myTurn !== myMove) {
        return false // it's not this player's turn
    }

    const elem = document.getElementById(`x${x}y${y}`)
    if (elem.innerHTML !== '') {
        return false // cell already occupied
    }

    elem.innerHTML = symbol
    setMyTurn(!myTurn)
    return true
}

function move(x, y) {
    if (!performMove(x, y, mySymbol)) {
        return
    }

    console.log(`sending move ${x} ${y}`)
    channel.send(JSON.stringify({
        type: 'move',
        value: {
            x,
            y,
        },
    }))

    checkGameOver()
}

function endGame() {
    channel.close()
    channel = null
    pc.close()
    pc = new RTCPeerConnection({iceServers: [{urls: 'stun:stun.l.google.com:19302'}]})
    switchUI('game-over', 'host-or-join')
}

relay.on('connect', () => {
    console.log(`connected to ${relay.url}`)

    if (inUI('wait-for-opponent')) {
        hostGameSubscribeNostrEvents()
    }
    if (inUI('joining')) {
        joinGamePublishNostrEvent()
    }
})
relay.on('error', () => {
    console.log(`failed to connect to ${relay.url}`)
})
relay.connect()

if (window.location.hash) {
    window.onload = () => {
        document.getElementById('opponent-pubkey').value = window.location.hash.substr(1)
        switchUI('host-or-join', 'join-opponent-pubkey')
        joining()
    }
}
